#!/bin/ash
VLAN=${VLAN:-0}

echo VLAN=$VLAN


ip link add link eth0 name eth0.$VLAN type vlan id $VLAN

ip addr flush dev eth0
ip -6 route del default

echo "disabling checksum offload"
ethtool --offload eth0 rx off tx off
ethtool --offload eth0.$VLAN rx off tx off

## disable IPv6 on the untagged interface, then enable it on the tagged one
#sysctl -w net.ipv6.conf.eth0.disable_ipv6=1
sysctl -w net.ipv6.conf.eth0.$VLAN.disable_ipv6=0
sysctl -w net.ipv6.conf.eth0.$VLAN.accept_ra=1
sysctl -w net.ipv6.conf.eth0.$VLAN.autoconf=1

ifconfig eth0.$VLAN up

sed -i s/eth0/eth0.$VLAN/ /etc/dhcpcd.conf

while true; do
  /sbin/dhcpcd -d -d -B eth0.$VLAN
  sleep 5
done
