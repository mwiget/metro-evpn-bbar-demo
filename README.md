## Lab to experiment with DHCP v4 & v6 redundancy over active - active EVPN

The lab is built using docker on a single root server with just one public interface. 

![network topo](network-topo.png)

### Requirements

- Baremetal server with Docker engine, ideally initialized via docker-machine. 
- Enable hugepages on the baremetal server 
- Enable loop kernel module (add loop to /etc/modules file). Needed to create config drive
- vmx-bundle-17.3R2.10.tgz or newer
- vmx-docker-light container (available via hub.docker.com and built automatically from git@gitlab.com:mwiget/vmx-docker-light.git)

Clone this repo plus dependent repo's to your local file system:

```
$ git clone git@git.juniper.net:mwiget/metro-evpn-bbar.git
$ cd metro-evpn-bbar
```

Unpack vmx-bundle-17.3B1.1.tgz (or newer) and copy the junos-vmx-x86-64-17.3B1.1.qcow2 file into the root directory of this repo.

[docker-compose.yml](docker-compose.yml) is used to create the virtual networks, launch BBAR1, BBAR2, CORE1 and the dhcpserver container. Don't forget '-d' to launch the containers in the background.
Or use 'make up'

```
$ make up
Creating metroevpnbbar_core1_1       ... done
Creating network "metroevpnbbar_net4-server" with the default driver
Creating metroevpnbbar_bbar1_1       ... done
Creating network "metroevpnbbar_net2-mpls" with the default driver
Creating metroevpnbbar_bbar2_1       ... done
Creating network "metroevpnbbar_net0-mgmt" with the default driver
Creating metroevpnbbar_core1_1       ...
Creating metroevpnbbar_bbar1_1       ...
Creating metroevpnbbar_dhcp4server_1 ... done
Creating metroevpnbbar_keadhcp6_1    ... done
Creating metroevpnbbar_dhcp4server_1 ...
Creating metroevpnbbar_dhcptester_1  ... done
Creating metroevpnbbar_dhcpclient_1  ... done
```

Use 'docker ps' and 'docker logs' to monitor progress or call 'make ps', which also calls
[getpass.sh](getpass.ps):

```
$ make ps
docker-compose ps
           Name                          Command               State               Ports
-----------------------------------------------------------------------------------------------------
metroevpnbbar_bbar1_1         /launch.sh                       Up      0.0.0.0:32819->22/tcp,
                                                                       0.0.0.0:32818->830/tcp
metroevpnbbar_bbar2_1         /launch.sh                       Up      0.0.0.0:32821->22/tcp,
                                                                       0.0.0.0:32820->830/tcp
metroevpnbbar_core1_1         /launch.sh                       Up      0.0.0.0:32817->22/tcp,
                                                                       0.0.0.0:32816->830/tcp
metroevpnbbar_dhcp4server_1   /sbin/tini /launch.sh            Up
metroevpnbbar_dhcpclient_1    /launch.sh                       Up
metroevpnbbar_dhcptester_1    /usr/bin/dhcptester -s 1 - ...   Up
metroevpnbbar_keadhcp6_1      /sbin/tini /launch.sh            Up
./getpass.sh
vMX metroevpnbbar_bbar2_1 (172.19.0.4) 17.3R2.10 koghoomakoojaiwahkoomeef 	 ...
vMX metroevpnbbar_bbar1_1 (172.19.0.3) 17.3R2.10 biphaichunoophoaquahlahc 	 ...
vMX metroevpnbbar_core1_1 (172.19.0.2) 17.3R2.10 oyohwoochohbeetieshohrey 	 ...
```

The '...' indicate, that the routers aren't fully operational yet, but running. Repeat the command until it says 'ready':

```
$ make ps
docker-compose ps
           Name                          Command               State               Ports
-----------------------------------------------------------------------------------------------------
metroevpnbbar_bbar1_1         /launch.sh                       Up      0.0.0.0:32819->22/tcp,
                                                                       0.0.0.0:32818->830/tcp
metroevpnbbar_bbar2_1         /launch.sh                       Up      0.0.0.0:32821->22/tcp,
                                                                       0.0.0.0:32820->830/tcp
metroevpnbbar_core1_1         /launch.sh                       Up      0.0.0.0:32817->22/tcp,
                                                                       0.0.0.0:32816->830/tcp
metroevpnbbar_dhcp4server_1   /sbin/tini /launch.sh            Up
metroevpnbbar_dhcpclient_1    /launch.sh                       Up
metroevpnbbar_dhcptester_1    /usr/bin/dhcptester -s 1 - ...   Up
metroevpnbbar_keadhcp6_1      /sbin/tini /launch.sh            Up
./getpass.sh
vMX metroevpnbbar_bbar2_1 (172.19.0.4) 17.3R2.10 koghoomakoojaiwahkoomeef 	 ready
vMX metroevpnbbar_bbar1_1 (172.19.0.3) 17.3R2.10 biphaichunoophoaquahlahc 	 ready
vMX metroevpnbbar_core1_1 (172.19.0.2) 17.3R2.10 oyohwoochohbeetieshohrey 	 ready
```

The vMX's are reachable via the IP address displayed next to each instance name.

Logging into bbar1 and display its neighbors, BGP peers and interface descriptions:

```
$ ssh 172.19.0.3
--- JUNOS 17.3R2.10 Kernel 64-bit  JNPR-10.3-20180204.bcafb2a_buil
mwiget@metroevpnbbar_bbar1_1> show interfaces descriptions
Interface       Admin Link Description
ge-0/0/0        up    up   metroevpnbbar_net1-subs
ge-0/0/1        up    up   metroevpnbbar_net2-mpls
ge-0/0/2        up    up   metroevpnbbar_net3-core
fxp0            up    up   metroevpnbbar_net0-mgmt

mwiget@metroevpnbbar_bbar1_1> show ipv6 neighbors
IPv6 Address                 Linklayer Address  State       Exp Rtr Secure Interface
2001:db8:3::2                02:42:0a:0a:00:02  reachable   27  yes no      ge-0/0/2.0
2001:db8:3::4                02:42:0a:0a:00:04  reachable   22  yes no      ge-0/0/2.0
fe80::42:acff:fe12:4         02:42:ac:12:00:04  stale       941 no  no      irb.1 [ge-0/0/0.1]
fe80::42:acff:fe12:6         02:42:ac:12:00:06  stale       927 no  no      irb.1 [ge-0/0/0.1]
fe80::42:acff:fe12:7         02:42:ac:12:00:07  stale       936 no  no      irb.1 [ge-0/0/0.1]
fe80::42:acff:fe12:8         02:42:ac:12:00:08  stale       944 no  no      irb.1 [ge-0/0/0.1]
fe80::42:acff:fe12:9         02:42:ac:12:00:09  stale       936 no  no      irb.1 [ge-0/0/0.1]
fe80::242:aff:fe02:3         02:42:0a:02:00:03  stale       719 yes no      ge-0/0/1.0
fe80::242:aff:fe0a:2         02:42:0a:0a:00:02  stale       670 yes no      ge-0/0/2.0
fe80::242:aff:fe0a:4         02:42:0a:0a:00:04  stale       699 yes no      ge-0/0/2.0

mwiget@metroevpnbbar_bbar1_1> show bgp summary
Groups: 2 Peers: 4 Down peers: 0
Table          Tot Paths  Act Paths Suppressed    History Damp State    Pending
inet.0
                      12          3          0          0          0          0
bgp.evpn.0
                      19          0          0          0          0          0
inet6.0
                      10          4          0          0          0          0
Peer                     AS      InPkt     OutPkt    OutQ   Flaps Last Up/Dwn State|#Active/Received/Accepted/Damped...
10.10.0.2        4200000001          4         17       0       0          28 Establ
  inet.0: 2/4/4/0
  bgp.evpn.0: 0/0/0/0
  evpn-mpls.evpn.0: 0/0/0/0
  __default_evpn__.evpn.0: 0/0/0/0
10.10.0.4        4200000001         15         17       0       0          17 Establ
  inet.0: 1/8/8/0
  bgp.evpn.0: 0/19/19/0
  evpn-mpls.evpn.0: 0/17/17/0
  __default_evpn__.evpn.0: 0/2/2/0
2001:db8:3::2    4200000001          6          3       0       0          18 Establ
  inet6.0: 4/6/5/0
2001:db8:3::4    4200000001          3          3       0       0           7 Establ
  inet6.0: 0/4/4/0

```

Show evpn database:

```
mwiget@metroevpnbbar_bbar1_1> show evpn database
Instance: evpn-mpls
VLAN  DomainId  MAC address        Active source                  Timestamp        IP address
1               00:05:86:84:69:f0  irb.1                          Jun 29 13:43:31  10.100.0.2
1               00:05:86:b3:19:f0  00:11:11:11:00:00:00:00:01:01  Jun 29 17:13:36  10.100.0.3
```

Now bring up a few clients using the dhcptester container on vlan 1 on net1-subs virtual network. This step is now already taken care of by the docker-compose file. You can attach to the dhcptester client using 

```
$ docker attach metroevpnbbar_dhcptester_1
     0      0      0      0      0      5      0      0    3960    0
```

You can launch additional clients on different or same SVLAN using:

```
$ docker run -ti --rm --name dhcptester --net metroevpnbbar_net1-subs marcelwiget/dhcptester -s 1 -i eth0 -n 10

dhcptester 1.0.16

10 clients on Interface eth0 802.1q (1)
802.1p marking set to Best Effort (0)
Max pending: 50 max retries: 10 interval: 4 seconds
Inter packet delay: 20 msec (-> max 50 cps)

Key Commands: (I)nit (B)ind (R)enew r(E)bind (L)ist B(o)und (A)rp (Q)uit

  INIT SELECT REQUES INITRE REBOOT  BOUND  RENEW REBIND RETRIES  CPS
     0      0      0      0      0     10      0      0       0   10

 Id clientMAC      serverMAC      routerMAC      clientIP  serverIP   routerIP   q-in-q
 1 0200.0000.0001 0005.86b3.19f0 0000.5e00.0101 10.100.0.10 10.99.0.100 10.100.0.1 (1) BOUND
 2 0200.0000.0002 0005.86b3.19f0 0000.5e00.0101 10.100.0.11 10.99.0.100 10.100.0.1 (1) BOUND
 3 0200.0000.0003 0005.86b3.19f0 0000.5e00.0101 10.100.0.12 10.99.0.100 10.100.0.1 (1) BOUND
 4 0200.0000.0004 0005.86b3.19f0 0000.5e00.0101 10.100.0.13 10.99.0.100 10.100.0.1 (1) BOUND
 5 0200.0000.0005 0005.86b3.19f0 0000.5e00.0101 10.100.0.14 10.99.0.100 10.100.0.1 (1) BOUND
 6 0200.0000.0006 0005.86b3.19f0 0000.5e00.0101 10.100.0.15 10.99.0.100 10.100.0.1 (1) BOUND
 7 0200.0000.0007 0005.86b3.19f0 0000.5e00.0101 10.100.0.16 10.99.0.100 10.100.0.1 (1) BOUND
 8 0200.0000.0008 0005.86b3.19f0 0000.5e00.0101 10.100.0.17 10.99.0.100 10.100.0.1 (1) BOUND
 9 0200.0000.0009 0005.86b3.19f0 0000.5e00.0101 10.100.0.18 10.99.0.100 10.100.0.1 (1) BOUND
 10 0200.0000.000a 0005.86b3.19f0 0000.5e00.0101 10.100.0.19 10.99.0.100 10.100.0.1 (1) BOUND
```

and check again on bbar1 and bbar2 the evpn database (and ping the clients):

```
mwiget@metroevpnbbar_bbar1_1> show evpn database
Instance: evpn-mpls
VLAN  DomainId  MAC address        Active source                  Timestamp        IP address
1               00:05:86:84:69:f0  irb.1                          Jun 29 13:43:31  10.100.0.2
1               00:05:86:b3:19:f0  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:42  10.100.0.3
1               02:00:00:00:00:01  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:02  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:03  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:04  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:05  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:06  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:07  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:08  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:09  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
1               02:00:00:00:00:0a  00:11:11:11:00:00:00:00:01:01  Jun 29 17:23:34
mwiget@metroevpnbbar_bbar1_1> ping 10.100.0.10
PING 10.100.0.10 (10.100.0.10): 56 data bytes
64 bytes from 10.100.0.10: icmp_seq=0 ttl=64 time=34.111 ms
64 bytes from 10.100.0.10: icmp_seq=1 ttl=64 time=8.186 ms
^C
--- 10.100.0.10 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max/stddev = 8.186/21.148/34.111/12.962 ms

mwiget@metroevpnbbar_bbar1_1> ping 10.100.0.19
PING 10.100.0.19 (10.100.0.19): 56 data bytes
64 bytes from 10.100.0.19: icmp_seq=0 ttl=64 time=40.388 ms
64 bytes from 10.100.0.19: icmp_seq=1 ttl=64 time=25.977 ms
^C
--- 10.100.0.19 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max/stddev = 25.977/33.182/40.388/7.206 ms
```

now same on bbar2:

```
mwiget@metroevpnbbar_bbar2_1> show evpn database
Instance: evpn-mpls
VLAN  DomainId  MAC address        Active source                  Timestamp        IP address
1               00:05:86:84:69:f0  00:11:11:11:00:00:00:00:01:02  Jun 29 17:17:21  10.100.0.2
1               00:05:86:b3:19:f0  irb.1                          Jun 29 15:48:31  10.100.0.3
1               02:00:00:00:00:01  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:15
1               02:00:00:00:00:02  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:03  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:04  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:05  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:06  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:07  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:08  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:09  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16
1               02:00:00:00:00:0a  00:11:11:11:00:00:00:00:01:02  Jun 29 17:22:16

mwiget@metroevpnbbar_bbar2_1> ping 10.100.0.10
PING 10.100.0.10 (10.100.0.10): 56 data bytes
64 bytes from 10.100.0.10: icmp_seq=0 ttl=64 time=27.525 ms
64 bytes from 10.100.0.10: icmp_seq=1 ttl=64 time=10.169 ms
^C
--- 10.100.0.10 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max/stddev = 10.169/18.847/27.525/8.678 ms

mwiget@metroevpnbbar_bbar2_1> ping 10.100.0.18
PING 10.100.0.18 (10.100.0.18): 56 data bytes
64 bytes from 10.100.0.18: icmp_seq=0 ttl=64 time=28.207 ms
64 bytes from 10.100.0.18: icmp_seq=1 ttl=64 time=11.671 ms
^C
--- 10.100.0.18 ping statistics ---
2 packets transmitted, 2 packets received, 0% packet loss
round-trip min/avg/max/stddev = 11.671/19.939/28.207/8.268 ms

mwiget@metroevpnbbar_bbar2_1>
```

Currently, I'm not using any subscriber state on the vMX's, just forward-only.

```
mwiget@metroevpnbbar_bbar1_1> show subscribers
Total subscribers: 0, Active Subscribers: 0
```

Log into core1 and ping the subscribers from here. The host routes are learned via BGP.
Todo: improve convergence time of BGP in this setup in case of BBARx failure.

```
mwiget@metroevpnbbar_core1_1> ping 10.100.0.12
PING 10.100.0.12 (10.100.0.12): 56 data bytes
64 bytes from 10.100.0.12: icmp_seq=0 ttl=63 time=55.552 ms
^C
--- 10.100.0.12 ping statistics ---
1 packets transmitted, 1 packets received, 0% packet loss
round-trip min/avg/max/stddev = 55.552/55.552/55.552/0.000 ms

mwiget@metroevpnbbar_core1_1> show route 10.100.0.12

inet.0: 14 destinations, 23 routes (14 active, 0 holddown, 0 hidden)
+ = Active Route, - = Last Active, * = Both

10.100.0.0/24      *[BGP/170] 01:37:44, localpref 100
                      AS path: I, validation-state: unverified
                    > to 10.10.0.4 via ge-0/0/0.0
                    [BGP/170] 03:44:03, localpref 100
                      AS path: I, validation-state: unverified
                    > to 10.10.0.3 via ge-0/0/0.0
```

### Simulate failures

This is very simple using [docker pause](https://docs.docker.com/engine/reference/commandline/pause/) command: This will freeze the specified container. Use 'docker unpause' to resume it. Works perfectly with the vMX containers.

### Nits and bits

Docker-compose and docker itself have an issue with network order. I fixed this within the vmx-docker-light container by checking the actual network assignments by docker from the running container and renaming the ethX interfaces accordingly. This probably also needs a fix to docker-compose, which I will eventually submit:

[https://github.com/mwiget/compose/tree/network-order](https://github.com/mwiget/compose/tree/network-order)

Actual issue with 'docker network connect':

Issue with network order: [https://github.com/docker/compose/issues/4645]()

The dhcpserver container used here must disable checksum offload. That is done within the container:

```
sudo ethtool --offload eth0.4 tx off
```

Example to stup a vlan (1) within linux (container) and assign an IP address:

```
ip link add link eth0 name eth0.1 type vlan id 1
ip link set up eth0.1
ifconfig eth0.1 10.100.0.101 netmask 255.255.255.0
```

