all: build up

build:
	docker-compose build

up: license-eval.txt id_rsa.pub
	docker-compose up -d

license-eval.txt:
	curl -o license-eval.txt https://www.juniper.net/us/en/dm/free-vmx-trial/E421992502.txt

id_rsa.pub:
	cp ~/.ssh/id_rsa.pub .

scale-5:
	docker-compose up -d --scale dhcpclient=5

scale-1:
	docker-compose up -d --scale dhcpclient=1 --scale dhcptester=0 

scale-0:
	docker-compose up -d --scale dhcpclient=0 --scale dhcptester=0

ps:
	docker-compose ps
	./getpass.sh

down:
	docker-compose down

clean:
	docker system prune -f
